var dotenv = require('dotenv');
dotenv.config();
var kafkaRouter = require('./routes/kafkaconsumer');
const WebSocket = require('websocket').client;

var ws = null;
var first_load = true;

const connectWebSocket = () => {
	const client = new WebSocket();
	client.on('connectFailed', function(error) {
	    console.log('Connect Error: ' + error.toString());
	    setTimeout(function(){connectWebSocket()}, 1000);
	});

	client.on('connect', function(connection) {
		if (connection.connected) {
			console.log("Connected");
			first_load = false;
			var subscribeAPI01 = {
				type: "subscribe",
				groupId: process.env.platform_id,
				topic: "dev-booking",
				key: process.env.platform_id
			};
			var subscribeAPI04 = {
				type: "subscribe",
				groupId: process.env.platform_id,
				topic: "dev-selectedOffer",
				key: process.env.platform_id
			};
			connection.sendUTF(JSON.stringify(subscribeAPI01));
			connection.sendUTF(JSON.stringify(subscribeAPI04));
		}
		connection.on('error', function(error) {
	        console.log("Connection Error: " + error.toString());
	        setTimeout(function(){connectWebSocket()}, 1000);
	    });
	    connection.on('close', function() {
	    	console.log("Connection Closed");
	        if ( first_load === false ) {
				setTimeout(function(){connectWebSocket()}, 1000);
			}
	    });
	    connection.on('message', function(message) {
	        if (message.type === 'utf8') {
	    		var contentMessage = message.utf8Data;
	    		var parsedMessage = JSON.parse(contentMessage);
	        	var status = parsedMessage.status || null;
				if ( status === "ok" ) {
					kafkaRouter.executeConsumerData(parsedMessage);
				}
	        }
	    });
	});
	client.connect('ws://'+process.env.websocket_server+':'+process.env.websocket_port, process.env.websocket_token);
};
connectWebSocket();
