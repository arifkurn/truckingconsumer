const API01 = require('../controllers').API01;
const API04 = require('../controllers').API04;

module.exports = {
	executeConsumerData(mainData) {
		var topic = mainData.topic || null;
		var data = mainData.data || null;

		switch(topic) {
			case 'nle-booking':
				API01.execute(data);
			break;
			case 'nle-selectedOffer':
				API04.execute(data);
			break;
			case 'dev-booking':
				API01.execute(data);
			break;
			case 'dev-selectedOffer':
				API04.execute(data);
			break;
		}
	}
};